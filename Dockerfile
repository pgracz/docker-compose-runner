FROM docker:latest

RUN apk add --update py-pip bash curl rsync

RUN pip install docker-compose==1.23.2 --upgrade pip
